# (c) 2013 Adam Pridgen adam.pridgen@thecoverofnight.com
# ida_pro_name_runtime_imports.py:
#        Rename references to runtime imports in a given function
# GPLv3 License
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
name: Name Win32 APIs addresses with the static address found in the binary.  For instance,
      if the binary built its own IAT, then the IAT can be rebuilt by supplying the addresses
      in the following format:

      dll_name,api_fn_name,address

'''

import idc


start = AskAddr(ScreenEA(), "Enter start address for scanning.")
end = AskAddr(ScreenEA(), "Enter start address for scanning.")
filename = AskFile(False, "", "Please select the file with the DLL, APIs, and Addresses")
word_sz = AskLong(4, "Please enter the word size.")


data = [[j.strip() for j in i.split(',')] for i in open(filename).read().splitlines()]

api_mapping = {}

for dll, api, addr in data:
    addr = int(addr, 16)
    api_mapping[addr] = [dll, api]

ea = start
while ea < end:
    value = idc.Dword(ea)
    if value in api_mapping:
        name = "_".join(api_mapping[value])
        idc.MakeName(ea, name)
    ea += word_sz
print "Finished"